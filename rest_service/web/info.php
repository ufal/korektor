<?php $main_page=basename(__FILE__); require('header.php') ?>

<?php require('about.php') ?>

<table class="table table-striped" cellpadding="4">
<tr>
    <th>Authors</th>
    <td>Michal Richter, Pavel Straňák, Milan Straka</td>
</tr>
<tr>
    <th>Homepage</th>
    <td><a href="http://ufal.mff.cuni.cz/korektor">http://ufal.mff.cuni.cz/korektor</a></td>
</tr>
<tr>
    <th>Development repository</th>
    <td><a href="https://redmine.ms.mff.cuni.cz/korektor.git">https://redmine.ms.mff.cuni.cz/korektor.git</a></td>
</tr>
<tr>
    <th>Status</th>
    <td>Beta</td>
</tr>
<tr>
    <th>OS</th>
    <td>Linux, Windows, Mac OS</td>
</tr>
<tr>
    <th>License of the library</th>
    <td><a href="http://opensource.org/licenses/BSD-2-Clause">BSD 2-Clause</a></td>
</tr>
<tr>
    <th>License of the models</th>
    <td><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/">CC BY-NC-SA</a></td>
</tr>
<tr>
    <th>Contact</th>
    <td><a href="mailto:stranak@ufal.mff.cuni.cz">stranak@ufal.mff.cuni.cz</a></td>
</tr>
</table>

<?php require('footer.php') ?>
