<p>Korektor is a statistical spell- and (occasional) grammar-checker.</p>

<p>This spellchecker strarted with Michal Richter's diploma thesis
<a href="https://redmine.ms.mff.cuni.cz/documents/1">Advanced Czech
Spellchecker</a>, but it is being developed further. There are two versions:
a unix command line utility (tested on Debian, Ubuntu and OS X) and an OS
X SpellServer with a System Service, that integrates with native OS X GUI
applications.</p>

<p>Copyright 2014 by Institute of Formal and Applied Linguistics, Faculty of
Mathematics and Physics, Charles University in Prague, Czech Republic.</p>
