#pragma once

#include "StdAfx.h"

struct FactorList {
	uint factors[4];
	float emission_costs[4];
};