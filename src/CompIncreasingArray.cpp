/*
Copyright (c) 2012, Charles University in Prague 
All rights reserved.
*/

#include "MyIncreasingArray.hpp"
#include "CompIncreasingArray.hpp"
#include "utils.hpp"

namespace ngramchecker {



		void CompIncreasingArray::WriteToStream(ostream &ofs) const
		{
			MyUtils::WriteString(ofs, "CIA");
			uint32_t num_parts = mia_vec.size();
			ofs.write((char*)&num_parts, sizeof(uint32_t));
			ofs.write((char*)&log2_size_of_parts, sizeof(uint32_t));
			ofs.write((char*)&num_values, sizeof(uint32_t));
			ofs.write((char*)&last_val_last_index, sizeof(uint32_t));

			for (uint32_t i = 0; i < mia_vec.size(); i++)
			{
				mia_vec[i]->WriteToStream(ofs);
			}
		}

		CompIncreasingArray::CompIncreasingArray(istream &ifs)
		{
			string check_string = MyUtils::ReadString(ifs);

			FATAL_CONDITION(check_string == "CIA", check_string);

			uint32_t num_parts;

			ifs.read((char*)&num_parts, sizeof(uint32_t));
			ifs.read((char*)&log2_size_of_parts, sizeof(uint32_t));
			ifs.read((char*)&num_values, sizeof(uint32_t));
			ifs.read((char*)&last_val_last_index, sizeof(uint32_t));

			for (uint32_t i = 0; i < num_parts; i++)
			{
				mia_vec.push_back(MyIncreasingArrayP(new MyIncreasingArray(ifs)));
			}

			bit_mask = (1 << log2_size_of_parts) - 1;
		}

		CompIncreasingArray::CompIncreasingArray(vector<uint32_t> &val, uint32_t _last_val_last_index)
		{
			num_values = val.size();
			log2_size_of_parts = 12;
			bit_mask = (1 << log2_size_of_parts) - 1;

			uint32_t first = 0;
			uint32_t last = min((int)(1 << log2_size_of_parts) - 1, (int)(val.size()) - 1);

			while (first < num_values)
			{
				vector<uint32_t> part;
				for (uint32_t i = first; i <= last; i++)
					part.push_back(val[i]);
				mia_vec.push_back(MyIncreasingArrayP(new MyIncreasingArray(part)));
				first += 1 << log2_size_of_parts;
				last += 1 << log2_size_of_parts;
				if (last >= num_values)
					last = num_values - 1;
			}

			for (uint32_t i = 0; i < num_values; i++)
			{
				FATAL_CONDITION(val[i] == GetValueAt(i), "CIA");
			}

			last_val_last_index = _last_val_last_index;
		}

}
